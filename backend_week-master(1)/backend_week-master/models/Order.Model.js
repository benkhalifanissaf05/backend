const mongoose = require('mongoose'); // Erase if already required

var orderSchema = new mongoose.Schema({
    price_Total: {
        type: String,
        required: true,
        unique: true,
        index: true,
    },
    ref: {
        type: String,
        required: true,

    },
    qte_Total: {
        type: String,
        required: true,

    },
    description: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    customer: { type: mongoose.Schema.Types.ObjectId, ref: "Customers" },
    delivery: { type: mongoose.Schema.Types.ObjectId, ref: "Deliveries" },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: "Products" }]


});
//Export the model
module.exports = mongoose.model('Products', orderSchema);