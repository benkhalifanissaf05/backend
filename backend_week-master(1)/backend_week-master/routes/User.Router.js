const UserController=require("../controllers/User.Controller")
const express=require("express")
const auth=require("../middlewares/auth")
const router=express.Router();

router.post("/login",UserController.login);
router.post("/logout",auth,UserController.logout);
router.post("/refresh",UserController.refresh);



module.exports=router;