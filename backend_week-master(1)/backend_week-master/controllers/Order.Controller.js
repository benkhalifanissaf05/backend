
const OrderModel = require("../models/Order.Model")

const OrderController = {

    create: function (req, res) {
        const Order = OrderModel(req.body);

        Order.save(function (err, item) {

            if (err) {

                console.log(err)
                res.json(err)
            }
            res.json(item)
        })



    },
    update: function (req, res) {
        const id = req.params.id;
        OrderModel.findByIdAndUpdate(id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    delete: function (req, res) {
        const id = req.params.id;
        OrderModel.findByIdAndDelete(id, {}, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    find: function (req, res) {

        OrderModel.find({}, function (err, items) {

            if (err) {
                res.json(err);
            }
            res.json(items)
        })

    },
    findById: function (req, res) {

        OrderModel.findOne({ _id: req.params.id }, function (err, item) {

            if (err) {
                res.json(err);
            }
            res.json(item)
        })

    }

}


module.exports = OrderController