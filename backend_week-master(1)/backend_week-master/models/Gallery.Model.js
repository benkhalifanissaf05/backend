const mongoose = require('mongoose'); // Erase if already required
// Declare the Schema of the Mongo model
var gallerySchema = new mongoose.Schema({
    url_photo: {
        type: String,
        required: true,
        unique: true,
        index: true,
    },
    product: { type: mongoose.Schema.Types.ObjectId, ref: "Products" }

});
//Export the model
module.exports = mongoose.model('Galleries', gallerySchema);