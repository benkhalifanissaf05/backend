const ProductModel = require("../models/Product.Model")
const ProductController = {

    create: function (req, res) {
        const Product = ProductModel(req.body);

        Product.save(function (err, item) {

            if (err) {

                console.log(err)
                res.json(err)
            }
            res.json(item)
        })



    },
    update: function (req, res) {
        const id = req.params.id;
        ProductModel.findByIdAndUpdate(id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    delete: function (req, res) {
        const id = req.params.id;
        ProductModel.findByIdAndDelete(id, {}, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    find: function (req, res) {

        ProductModel.find({}, function (err, items) {

            if (err) {
                res.json(err);
            }
            res.json(items)
        })

    },
    findById: function (req, res) {

        ProductModel.findOne({ _id: req.params.id }, function (err, item) {

            if (err) {
                res.json(err);
            }
            res.json(item)
        })

    }

}


module.exports = ProductController