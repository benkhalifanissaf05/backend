const ProviderController=require("../controllers/Provider.Controller")
const express=require("express")
const router=express.Router();

router.post("/",ProviderController.create);
router.put("/:id",ProviderController.update);
router.delete("/:id",ProviderController.delete);
router.get("/",ProviderController.find);
router.get("/:id",ProviderController.findById);


module.exports=router;