const mongoose = require('mongoose'); // Erase if already required

var SubcategorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        index: true,
    },
    description: {
        type: String,
        required: true,

    },
    category:
    {
        type: mongoose.Schema.Types.ObjectId, ref: "Categories"
    },
    products:
        [{
            type: mongoose.Schema.Types.ObjectId, ref: "Products"
        }]


});
//Export the model
module.exports = mongoose.model('SubCategories', SubcategorySchema);