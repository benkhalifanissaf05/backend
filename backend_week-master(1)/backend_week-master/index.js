const express = require('express')
const cors = require('cors')

var nodemailer = require('nodemailer');
var fs=require("fs")
var hogan = require('hogan.js');
var inlineCss = require('inline-css');


const userRouter = require("./routes/User.Router")
const customerRouter = require("./routes/Customer.Router")
const providerRouter = require("./routes/Provider.Router")
const deliveryRouter = require("./routes/Delivery.Router")
const categoryRouter = require("./routes/Category.Router")
const subcategoryRouter = require("./routes/SubCategory.Router")
const auth = require("./middlewares/auth")
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
/**
  call database
 */
const db = require("./config/db")
const app = express()
app.use(cors("*"))
app.set("secretKey", "itgatetraining")
app.use(express.json())

app.use("/users", userRouter);
app.use("/categories", auth, categoryRouter)
app.use("/subcategories", auth, subcategoryRouter)
app.use("/customers", customerRouter)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.post("/sendMail", async function (req, res) {

    var transporter = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
            user: "59f01c77de1380",
            pass: "470725594c38f9"
        }
    });

      //Load the template file
      const templateFile = fs.readFileSync("./templates/template.html");
      //Load and inline the style
      const templateStyled = await inlineCss(templateFile.toString(), {url: "file://"+__dirname+"/templates/"});
      //Inject the data in the template and compile the html
      const templateCompiled = hogan.compile(templateStyled);
      const templateRendered = templateCompiled.render({text: "HelloWorld"});
 

    var mailOptions = {
        from: req.body.from,
        to: req.body.to,
        subject: req.body.subject,
        //   text: req.body.text,
        html: templateRendered ,

        attachments: [

            {   // use URL as an attachment
                filename: req.body.file,
                path: 'http://localhost:3000/readFile/' + req.body.file,
                cid: 'logo'
            }


        ]

    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            res.json(info.response)
        }
    });

})
app.get("/readFile/:img", function (req, res) {

    res.sendFile(__dirname + "/uploads/" + req.params.img)

})


const port = 3000
app.get('/', (req, res) => {
    res.send('Hello World!')
})
app.use("/users", userRouter)
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})

