const mongoose = require('mongoose'); // Erase if already required

var categorySchema = new mongoose.Schema({

    name: {
        type: String,
        required: true,
        unique: true,
        index: true,
    },
    description: {
        type: String,
        required: true,

    },
    subcategories: [
        {
            type: mongoose.Schema.Types.ObjectId, ref: "SubCategories"
        }
    ]

});
//Export the model
module.exports = mongoose.model('Categories', categorySchema);