const CategoryController=require("../controllers/Category.Controller")
const express=require("express")
const router=express.Router();

router.post("/",CategoryController.create);
router.put("/:id",CategoryController.update);
router.delete("/:id",CategoryController.delete);
router.get("/",CategoryController.find);
router.get("/:id",CategoryController.findById);


module.exports=router;