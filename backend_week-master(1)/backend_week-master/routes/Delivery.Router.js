const DeliveryController=require("../controllers/Delivery.Controller")
const express=require("express")
const router=express.Router();

router.post("/",DeliveryController.create);
router.put("/:id",DeliveryController.update);
router.delete("/:id",DeliveryController.delete);
router.get("/",DeliveryController.find);
router.get("/:id",DeliveryController.findById);


module.exports=router;