const CustomerModel = require("../models/Customer.Model")
const CustomerController = {

    register: function (req, res) {
        req.body["photo"] = req.file.filename;
        const customer = CustomerModel(req.body);
        customer.save(function (err, item) {
            if (err) {
                console.log(err)
                res.json(err)
            }
            res.json(item)
        })
    },
    update: function (req, res) {
        const id = req.params.id;
        CustomerModel.findByIdAndUpdate(id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    delete: function (req, res) {
        const id = req.params.id;
        CustomerModel.findByIdAndDelete(id, {}, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    find: function (req, res) {

        CustomerModel.find({}, function (err, items) {

            if (err) {
                res.json(err);
            }
            res.json(items)
        })

    },
    findById: function (req, res) {

        CustomerModel.findOne({ _id: req.params.id }, function (err, item) {

            if (err) {
                res.json(err);
            }
            res.json(item)
        })

    }
}


module.exports = CustomerController