const GalleryModel = require("../models/Gallery.Model")
const GalleryController = {

    create: function (req, res) {
        const Gallery = GalleryModel(req.body);

        Gallery.save(function (err, item) {

            if (err) {

                console.log(err)
                res.json(err)
            }
            res.json(item)
        })



    },
    update: function (req, res) {
        const id = req.params.id;
        GalleryModel.findByIdAndUpdate(id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    delete: function (req, res) {
        const id = req.params.id;
        GalleryModel.findByIdAndDelete(id, {}, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    find: function (req, res) {

        GalleryModel.find({}, function (err, items) {

            if (err) {
                res.json(err);
            }
            res.json(items)
        })

    },
    findById: function (req, res) {

        GalleryModel.findOne({ _id: req.params.id }, function (err, item) {

            if (err) {
                res.json(err);
            }
            res.json(item)
        })

    }

}


module.exports = GalleryController