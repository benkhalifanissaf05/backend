const CustomerController = require("../controllers/Customer.Controller");
const express = require("express");
const router = express.Router();
const upload = require("../middlewares/uploads");

router.post("/", upload.single("file"), CustomerController.register);
router.put("/:id", upload.single("file"), CustomerController.update);
router.delete("/:id", CustomerController.delete);
router.get("/", CustomerController.find);
router.get("/:id", CustomerController.findById);

module.exports = router;