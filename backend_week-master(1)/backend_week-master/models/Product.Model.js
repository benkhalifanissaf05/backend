const mongoose = require('mongoose'); // Erase if already required

var productSchema = new mongoose.Schema({
    prix: {
        type: String,
        required: true,
        unique: true,
        index: true,
    },
    reference: {
        type: String,
        required: true,

    },
    qte: {
        type: String,
        required: true,

    },
    description: {
        type: String,
        required: true
    },
    subcategory: { type: mongoose.Schema.Types.ObjectId, ref: "SubCategories" },
    provider: { type: mongoose.Schema.Types.ObjectId, ref: "Providers" },
    galleries: [{ type: mongoose.Schema.Types.ObjectId, ref: "Galleries" }],
    orders: [{ type: mongoose.Schema.Types.ObjectId, ref: "Orders" }]


});
//Export the model
module.exports = mongoose.model('Products', productSchema);