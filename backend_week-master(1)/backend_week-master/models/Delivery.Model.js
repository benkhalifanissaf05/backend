const UserModel = require("./User.Model")
var mongoose = require('mongoose');
var deliverySchema = new mongoose.Schema({
    adress: { type: String, required: false, index: false },
    orders: [{ type: mongoose.Schema.Types.ObjectId, ref: "Orders" }]

});

UserModel.discriminator("Deliveries", deliverySchema);
module.exports = mongoose.model('Deliveries');