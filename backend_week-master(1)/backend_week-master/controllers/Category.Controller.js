const CategoryModel = require("../models/Category.Model")
const CategoryController = {

    create: function (req, res) {

        console.log('%cCategory.Controller.js line:6 req.body', 'color: #007acc;', req.body);

        const Category = CategoryModel(req.body);

        Category.save(function (err, item) {

            if (err) {

                console.log(err)
                res.json(err)
            }
            res.json(item)
        })



    },
    update: function (req, res) {
        const id = req.params.id;
        CategoryModel.findByIdAndUpdate(id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    delete: function (req, res) {
        const id = req.params.id;
        CategoryModel.findByIdAndDelete(id, {}, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    find: function (req, res) {

        CategoryModel.find({}, function (err, items) {

            if (err) {
                res.json(err);
            }
            res.json(items)
        })

    },
    findById: function (req, res) {

        CategoryModel.findOne({ _id: req.params.id }, function (err, item) {

            if (err) {
                res.json(err);
            }
            res.json(item)
        })

    }

}


module.exports = CategoryController