const GalleryController=require("../controllers/Gallery.Controller")
const express=require("express")
const router=express.Router();

router.post("/",GalleryController.create);
router.put("/:id",GalleryController.update);
router.delete("/:id",GalleryController.delete);
router.get("/",GalleryController.find);
router.get("/:id",GalleryController.findById);


module.exports=router;