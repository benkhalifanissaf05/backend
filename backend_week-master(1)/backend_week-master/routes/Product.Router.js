const ProductController=require("../controllers/Product.Controller")
const express=require("express")
const router=express.Router();

router.post("/",ProductController.create);
router.put("/:id",ProductController.update);
router.delete("/:id",ProductController.delete);
router.get("/",ProductController.find);
router.get("/:id",ProductController.findById);


module.exports=router;