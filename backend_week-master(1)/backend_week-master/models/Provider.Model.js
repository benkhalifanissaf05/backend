const UserModel = require("./User.Model")
var mongoose = require('mongoose');
var providerSchema = new mongoose.Schema({
    company: { type: String, required: false, index: false },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: "Products" }]
});
UserModel.discriminator("Provider", providerSchema);
module.exports = mongoose.model('Provider');