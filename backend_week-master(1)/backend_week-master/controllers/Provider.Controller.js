
const ProviderModel = require("../models/Provider.Model")

const ProviderController = {

    create: function (req, res) {
        const Provider = ProviderModel(req.body);

        Provider.save(function (err, item) {

            if (err) {

                console.log(err)
                res.json(err)
            }
            res.json(item)
        })



    },
    update: function (req, res) {
        const id = req.params.id;
        ProviderModel.findByIdAndUpdate(id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    delete: function (req, res) {
        const id = req.params.id;
        ProviderModel.findByIdAndDelete(id, {}, function (err, item) {
            if (err) {
                res.json(err)
            }
            res.json(item)
        })
    },
    find: function (req, res) {

        ProviderModel.find({}, function (err, items) {

            if (err) {
                res.json(err);
            }
            res.json(items)
        })

    },
    findById: function (req, res) {

        ProviderModel.findOne({ _id: req.params.id }, function (err, item) {

            if (err) {
                res.json(err);
            }
            res.json(item)
        })

    }

}


module.exports = ProviderController