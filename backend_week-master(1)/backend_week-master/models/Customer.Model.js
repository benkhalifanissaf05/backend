const UserModel = require("./User.Model")
var mongoose = require('mongoose');
var customerSchema = new mongoose.Schema({
    localization: { type: String, required: false, index: false },
    orders: [{ type: mongoose.Schema.Types.ObjectId, ref: "Orders" }]

});

UserModel.discriminator("Customers", customerSchema)
module.exports = mongoose.model('Customers');