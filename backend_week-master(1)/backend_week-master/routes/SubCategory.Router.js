const SubCategoryController=require("../controllers/SubCategory.Controller")
const express=require("express")
const router=express.Router();

router.post("/",SubCategoryController.create);
router.put("/:id",SubCategoryController.update);
router.delete("/:id",SubCategoryController.delete);
router.get("/",SubCategoryController.find);
router.get("/:id",SubCategoryController.findById);


module.exports=router;